/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicola Baldo <nbaldo@cttc.es>
 */

#ifndef EPC_ENB_RRC_SAP_H
#define EPC_ENB_RRC_SAP_H

#include <list>
#include <stdint.h>
#include <ns3/eps-bearer.h>
#include <ns3/ipv4-address.h>
#include <ns3/ptr.h>
#include <ns3/simulator.h>


namespace ns3 {

class Packet;

class EpcEnbRrcSapProvider

{
public:
	virtual ~EpcEnbRrcSapProvider ();
	virtual void SendToRRCSocket (Ptr<Packet> packet, Ipv4Address enbAddr) = 0;

};




class EpcEnbRrcSapUser
{
public:
  virtual ~EpcEnbRrcSapUser ();


};



template <class C>
class MemberEpcEnbRrcSapProvider : public EpcEnbRrcSapProvider
{
public:
	MemberEpcEnbRrcSapProvider (C* owner);

	virtual void SendToRRCSocket (Ptr<Packet> packet, Ipv4Address enbAddr);

private:
  MemberEpcEnbRrcSapProvider ();
  C* m_owner;
};

template <class C>
MemberEpcEnbRrcSapProvider<C>::MemberEpcEnbRrcSapProvider (C* owner)
  : m_owner (owner)
{
}

template <class C>
MemberEpcEnbRrcSapProvider<C>::MemberEpcEnbRrcSapProvider ()
{
}

template <class C>
void MemberEpcEnbRrcSapProvider<C>::SendToRRCSocket (Ptr<Packet> packet, Ipv4Address enbAddr)
{
  m_owner->SendToRRCSocket (packet, enbAddr);
}


template <class C>
class MemberEpcEnbRrcSapUser : public EpcEnbRrcSapUser
{
public:
	MemberEpcEnbRrcSapUser (C* owner);

private:
  MemberEpcEnbRrcSapUser ();
  C* m_owner;
};

template <class C>
MemberEpcEnbRrcSapUser<C>::MemberEpcEnbRrcSapUser (C* owner)
  : m_owner (owner)
{
}

template <class C>
MemberEpcEnbRrcSapUser<C>::MemberEpcEnbRrcSapUser ()
{
}


} // namespace ns3

#endif // EPC_ENB_RRC_SAP_H
