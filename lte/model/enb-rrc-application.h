/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Jaume Nin <jnin@cttc.cat>
 *         Nicola Baldo <nbaldo@cttc.cat>
 */

#ifndef ENB_RRC_APPLICATION_H
#define ENB_RRC_APPLICATION_H

#include <ns3/address.h>
#include <ns3/socket.h>
#include <ns3/virtual-net-device.h>
#include <ns3/traced-callback.h>
#include <ns3/callback.h>
#include <ns3/ptr.h>
#include <ns3/object.h>
#include <ns3/eps-bearer.h>
#include <ns3/epc-tft.h>
#include <ns3/epc-tft-classifier.h>
#include <ns3/lte-common.h>
#include <ns3/application.h>
#include <ns3/epc-s1ap-sap.h>
#include <ns3/epc-s11-sap.h>
#include <map>


#include <ns3/lte-enb-cmac-sap.h>
#include <ns3/lte-mac-sap.h>
#include <ns3/ff-mac-sched-sap.h>
#include <ns3/ff-mac-csched-sap.h>
#include <ns3/lte-pdcp-sap.h>
#include <ns3/epc-x2-sap.h>
#include <ns3/epc-enb-s1-sap.h>
#include <ns3/lte-handover-management-sap.h>
#include <ns3/lte-enb-cphy-sap.h>
#include <ns3/lte-rrc-sap.h>
#include <ns3/lte-anr-sap.h>
#include <ns3/lte-ffr-rrc-sap.h>
#include <ns3/lte-enb-rrc.h>
#include <ns3/net-device.h>
#include <ns3/net-device-container.h>
#include <ns3/node.h>
#include <ns3/node-container.h>

#include <ns3/point-to-point-helper.h>

#include "epc-enb-rrc-sap.h"
#include "lte-rrc-sap.h"



//#include "lte-enb-rrc.h"

namespace ns3 {

class EpcEnbApplication;
class LteRadioBearerInfo;
class LteSignalingRadioBearerInfo;
class LteDataRadioBearerInfo;
class LteEnbRrc;
class Packet;
class UeManager;

//extern Ptr<LteEnbRrc> rrc;
//extern uint16_t temp_rnti;

class EnbRrcApplication : public Application
{
	//friend class MemberEpcEnbSplitRrcSapProvider<EpcEnbApplication>;

	//friend class MemberLteEnbRrcSapUser<EnbRrcApplication>;
	//friend class MemberEpcEnbSplitRrcSapUser<EnbRrcApplication>;
/*friend	class EpcEnbApplication;
friend class LteRadioBearerInfo;
friend class LteSignalingRadioBearerInfo;
friend class LteDataRadioBearerInfo;
friend class LteEnbRrc;
friend class Packet;*/
friend class UeManager;
friend class LteEnbRrc;
friend class MemberEpcEnbRrcSapProvider<EnbRrcApplication>;
//friend class MemberEpcS1apSapEnb<EpcEnbApplication>;

//friend class MemberEpcEnbSplitRrcSapProvider<EnbRrcApplication>;



public:

//EnbRrcApplication (const Ptr<Socket> rrcSocket, Ptr<LteEnbRrc> rrc);
	EnbRrcApplication (const Ptr<Socket> rrcSocket);

  /**
   * Destructor
   */
  virtual ~EnbRrcApplication (void);

  // inherited from Object
  static TypeId GetTypeId (void);
  virtual void DoDispose ();


  /*void SetEpcEnbSplitRrcSapProvider (EpcEnbSplitRrcSapProvider* p);
  EpcEnbSplitRrcSapUser* GetEpcEnbSplitRrcSapUser ();*/
/*
  void SetEpcEnbSplitRrcSapUser (EpcEnbSplitRrcSapUser * s);
  EpcEnbSplitRrcSapProvider* GetEpcEnbSplitRrcSapProvider ();*/

  /**
   * Constructor that binds the tap device to the callback methods.
   *
   * \param tunDevice TUN VirtualNetDevice used to tunnel IP packets from
   * the Gi interface of the PGW/SGW over the
   * internet over GTP-U/UDP/IP on the S1-U interface
   * \param s1uSocket socket used to send GTP-U packets to the eNBs
   */

/*

  void SetEnbRrcSapUser (EpcEnbRrcSapUser * s);
  EpcEnbRrcSapProvider* GetEnbRrcSapProvider ();*/

   /**
    *
    * \return the S1 SAP Provider
    */


  //////////////////////////////////////////////////////////////////////////////////

/*  //virtual void DoCompleteSetupUe (uint16_t rnti, EpcEnbSplitRrcSapProvider::CompleteSetupUeParameters params);
    virtual void DoRecvRrcConnectionRequest (uint16_t rnti, LteRrcSap::RrcConnectionRequest msg);
    virtual void DoRecvRrcConnectionSetupCompleted (uint16_t rnti, LteRrcSap::RrcConnectionSetupCompleted msg);
    virtual void DoRecvRrcConnectionReconfigurationCompleted (uint16_t rnti, LteRrcSap::RrcConnectionReconfigurationCompleted msg);
    virtual void DoRecvRrcConnectionReestablishmentRequest (uint16_t rnti, LteRrcSap::RrcConnectionReestablishmentRequest msg);
    virtual void DoRecvRrcConnectionReestablishmentComplete (uint16_t rnti, LteRrcSap::RrcConnectionReestablishmentComplete msg);
    virtual void DoRecvMeasurementReport (uint16_t rnti, LteRrcSap::MeasurementReport msg);
    virtual void DoRecvRRCSocket(uint16_t rnti,Ptr<Packet> p);




   EpcEnbSplitRrcSapProvider* m_enbsplitRrcSapProvider;
   EpcEnbSplitRrcSapUser* m_enbsplitRrcSapUser;*/




  /**
   * Method to be assigned to the callback of the Gi TUN VirtualNetDevice. It
   * is called when the SGW/PGW receives a data packet from the
   * internet (including IP headers) that is to be sent to the UE via
   * its associated eNB, tunneling IP over GTP-U/UDP/IP.
   *
   * \param packet
   * \param source
   * \param dest
   * \param protocolNumber
   * \return true always
   */
  //bool RecvFromTunDevice (Ptr<Packet> packet, const Address& source, const Address& dest, uint16_t protocolNumber);


  /**
   * Method to be assigned to the recv callback of the S1-U socket. It
   * is called when the SGW/PGW receives a data packet from the eNB
   * that is to be forwarded to the internet.
   *
   * \param socket pointer to the S1-U socket
   */
  //void RecvFromS1uSocket (Ptr<Socket> socket);


  void SetRrcSapUser (EpcEnbRrcSapUser * s);
    EpcEnbRrcSapProvider* GetRrcSapProvider ();


  void AddEnb (uint16_t cellId, Ipv4Address enbAddr, Ipv4Address rrcAddr, Ptr<LteEnbRrc> rrc );

  void RecvFromRRCSocket (Ptr<Socket> socket);

  //void srcSocketRecv (Ptr<Socket> socket);
  /**
   * Send a packet to the internet via the Gi interface of the SGW/PGW
   *
   * \param packet
   */
  //void SendToTunDevice (Ptr<Packet> packet, uint32_t teid);


  /**
   * Send a packet to the SGW via the S1-U interface
   *
   * \param packet packet to be sent
   * \param enbS1uAddress the address of the eNB
   * \param teid the Tunnel Enpoint IDentifier
   */
  //void SendToS1uSocket (Ptr<Packet> packet, Ipv4Address enbS1uAddress);

  void SendToRRCSocket (Ptr<Packet> packet, Ipv4Address enbAddress_rrc);

/**
   * Set the MME side of the S11 SAP 
   * 
   * \param s the MME side of the S11 SAP 
   */
  //void SetS11SapMme (EpcS11SapMme * s);

  /** 
   * 
   * \return the SGW side of the S11 SAP 
   */
  //EpcS11SapSgw* GetS11SapSgw ();


  /** 
   * Let the SGW be aware of a new eNB 
   * 
   * \param cellId the cell identifier
   * \param enbAddr the address of the eNB
   * \param sgwAddr the address of the SGW
   */

  //void AddEnb (uint16_t cellId, Ipv4Address enbAddr, Ipv4Address sgwAddr);

  /** 
   * Let the SGW be aware of a new UE
   * 
   * \param imsi the unique identifier of the UE
   */
  //void AddUe (uint64_t imsi);

  /** 
   * set the address of a previously added UE
   * 
   * \param imsi the unique identifier of the UE
   * \param ueAddr the IPv4 address of the UE
   */
  //void SetUeAddress (uint64_t imsi, Ipv4Address ueAddr);

/*
 *
 *
private:

  // S11 SAP SGW methods
  void DoCreateSessionRequest (EpcS11SapSgw::CreateSessionRequestMessage msg);
  void DoModifyBearerRequest (EpcS11SapSgw::ModifyBearerRequestMessage msg);  

  void DoDeleteBearerCommand (EpcS11SapSgw::DeleteBearerCommandMessage req);
  void DoDeleteBearerResponse (EpcS11SapSgw::DeleteBearerResponseMessage req);


  *
   * store info for each UE connected to this SGW

  class UeInfo : public SimpleRefCount<UeInfo>
  {
public:
    UeInfo ();  

    *
     * 
     * \param tft the Traffic Flow Template of the new bearer to be added
     * \param epsBearerId the ID of the EPS Bearer to be activated
     * \param teid  the TEID of the new bearer

    void AddBearer (Ptr<EpcTft> tft, uint8_t epsBearerId, uint32_t teid);

    *
     * \brief Function, deletes contexts of bearer on SGW and PGW side
     * \param bearerId, the Bearer Id whose contexts to be removed

    void RemoveBearer (uint8_t bearerId);

    *
     * 
     * 
     * \param p the IP packet from the internet to be classified
     * 
     * \return the corresponding bearer ID > 0 identifying the bearer
     * among all the bearers of this UE;  returns 0 if no bearers
     * matches with the previously declared TFTs

    uint32_t Classify (Ptr<Packet> p);

    *
     * \return the address of the eNB to which the UE is connected

    Ipv4Address GetEnbAddr ();

    *
     * set the address of the eNB to which the UE is connected
     * 
     * \param addr the address of the eNB

    void SetEnbAddr (Ipv4Address addr);

    *
     * \return the address of the UE

    Ipv4Address GetUeAddr ();

    *
     * set the address of the UE
     * 
     * \param addr the address of the UE

    void SetUeAddr (Ipv4Address addr);


  private:
    EpcTftClassifier m_tftClassifier;
    Ipv4Address m_enbAddr;
    Ipv4Address m_ueAddr;
    std::map<uint8_t, uint32_t> m_teidByBearerIdMap;
  };
*/


 /**
  * UDP socket to send and receive GTP-U packets to and from the S1-U interface
  */
  //Ptr<Socket> m_s1uSocket;
  
   Ptr<Socket> m_rrcSocket;


  /**
   * TUN VirtualNetDevice used for tunneling/detunneling IP packets
   * from/to the internet over GTP-U/UDP/IP on the S1 interface 
   */
  //Ptr<VirtualNetDevice> m_tunDevice;

  /**
   * Map telling for each UE address the corresponding UE info 
   */
  //std::map<Ipv4Address, Ptr<UeInfo> > m_ueInfoByAddrMap;

  /**
   * Map telling for each IMSI the corresponding UE info 
   */
  //std::map<uint64_t, Ptr<UeInfo> > m_ueInfoByImsiMap;

  /**
   * UDP port to be used for GTP
   */
  uint16_t m_gtpuUdpPort;

  uint16_t m_enbRRCPort;
  Ptr<LteEnbRrc> new_rrc;
  EpcEnbRrcSapUser* m_enbrrcSapUser_new;
  EpcEnbRrcSapProvider* m_enbrrcSapProvider_new;

  /*EpcEnbRrcSapProvider* m_epcenbrrcSapProvider;



    *
     * User for the S1 SAP

  EpcEnbRrcSapUser* m_epcenbrrcSapUser;

  LteEnbRrcSapProvider* m_enbrrcsapprovider;*/



  Ptr<EpcEnbApplication> m_enbEpcApp;

 // Ptr<LteEnbRrc> m_rrc;
 // ObjectFactory m_enbNetDeviceFactory;
//  NetDeviceContainer devices;


  //Ptr<UeManager> m_uem;
 // Ptr<UeManager> GetUeManager (uint16_t rnti);

  //uint32_t m_teidCount;

  /**
   * MME side of the S11 SAP
   * 
   */
  //EpcS11SapMme* m_s11SapMme;

  /**
   * SGW side of the S11 SAP
   * 
   */
  //EpcS11SapSgw* m_s11SapSgw;




  struct EnbInfo
  {
    Ipv4Address enbAddr;
    Ipv4Address rrcAddr;
  };

  std::map<uint16_t, EnbInfo> m_enbInfoByCellId;


 // Ptr<LteEnbRrc> new_rrc1;

  //LteEnbRrcSapUser* m_rrcSapUser;
//  std::map<uint16_t, LteEnbRrcSapUser::SetupUeParameters> m_setupUeParametersMap;
  std::map<Ipv4Address, Ptr<LteEnbRrc> > enb_ip_rrc_ptr;
};







} //namespace ns3

#endif

