/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Manuel Requena <manuel.requena@cttc.es>
 */

#include "ns3/log.h"

#include "lte-split-rrc-header.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("RntiHeader");

NS_OBJECT_ENSURE_REGISTERED (RntiHeader);


RntiHeader::RntiHeader ()
{
}

RntiHeader::~RntiHeader ()
{
}


void
RntiHeader::SetRntiData (uint16_t data)
{
	rnti_data = data;
}
uint16_t
RntiHeader::GetRntiData (void)
{
  return rnti_data;
}

void
RntiHeader::SetMsgType (uint16_t data)
{
	msg_type = data;
}
uint16_t
RntiHeader::GetMsgType (void)
{
  return msg_type;
}



void
RntiHeader::SetUlDl (uint16_t data)
{
	ul_dl_type = data;
}
uint16_t
RntiHeader::GetUlDl (void)
{
  return ul_dl_type;
}


void
RntiHeader::Setlcid (uint8_t data)
{
	lcid_value = data;
}
uint8_t
RntiHeader::Getlcid (void)
{
  return lcid_value;
}


void
RntiHeader::SetRejectWaitTime  (uint8_t data)
{
	reject_wait_time = data;
}
uint8_t
RntiHeader::GetRejectWaitTime (void)
{
  return reject_wait_time;
}




TypeId
RntiHeader::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::RntiHeader")
    .SetParent<Header> ()
	.SetGroupName("Lte")

    ;
  return tid;
}

TypeId
RntiHeader::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
RntiHeader::GetSerializedSize (void) const
{
  // two bytes of data to store
  return 8;
}
void
RntiHeader::Serialize (Buffer::Iterator start) const
{
  start.WriteHtonU16 (rnti_data);
  start.WriteHtonU16(msg_type);
  start.WriteHtonU16(ul_dl_type);
  start.WriteU8(lcid_value);
  start.WriteU8(reject_wait_time);

}
uint32_t
RntiHeader::Deserialize (Buffer::Iterator start)
{
	rnti_data = start.ReadNtohU16 ();
	msg_type = start.ReadNtohU16 ();
	ul_dl_type = start.ReadNtohU16();
	lcid_value = start.ReadU8();
	reject_wait_time = start.ReadU8();
  return GetSerializedSize();
}
void
RntiHeader::Print (std::ostream &os) const
{
  os << rnti_data;
  os << msg_type;
  os << ul_dl_type;
 os << lcid_value;
 os << reject_wait_time;
}




}; // namespace ns3
