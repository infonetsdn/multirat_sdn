/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Jaume Nin <jnin@cttc.cat>
 *         Nicola Baldo <nbaldo@cttc.cat>
 */


#include "enb-rrc-application.h"
#include "ns3/log.h"
#include "ns3/mac48-address.h"
#include "ns3/ipv4.h"
#include "ns3/inet-socket-address.h"
#include "ns3/epc-gtpu-header.h"
#include "ns3/abort.h"
#include "epc-enb-application.h"
#include "lte-rrc-protocol-real.h"
#include "lte-ue-rrc.h"
#include "lte-enb-rrc.h"
#include "lte-rrc-protocol-ideal.h"
#include <ns3/simulator.h>
#include <ns3/lte-radio-bearer-info.h>
#include <ns3/lte-enb-net-device.h>
#include <typeinfo>
#include <string>
#include "lte-split-rrc-header.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("EnbRrcApplication");

/////////////////////////
// EnbRrcApplication
/////////////////////////

EnbRrcApplication::EnbRrcApplication (const Ptr<Socket> rrcSocket)
	: m_rrcSocket (rrcSocket),
	m_enbRRCPort  (2153), // fixed by the standard
	m_enbrrcSapUser_new (0)
{

  NS_LOG_FUNCTION (this << rrcSocket);
  m_rrcSocket->SetRecvCallback (MakeCallback (&EnbRrcApplication::RecvFromRRCSocket, this));
  m_enbrrcSapProvider_new = new MemberEpcEnbRrcSapProvider<EnbRrcApplication> (this);
}


EnbRrcApplication::~EnbRrcApplication ()
{
  NS_LOG_FUNCTION (this);
}


void
EnbRrcApplication::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  m_rrcSocket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
  m_rrcSocket = 0;
  delete m_enbrrcSapProvider_new;
  delete m_enbrrcSapUser_new;
}


TypeId
EnbRrcApplication::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::EnbRrcApplication")
    .SetParent<Object> ()
	.SetGroupName("Lte")
	;
  return tid;
}

void
EnbRrcApplication::SetRrcSapUser (EpcEnbRrcSapUser * s)
{
	m_enbrrcSapUser_new = s;
}


EpcEnbRrcSapProvider*
EnbRrcApplication::GetRrcSapProvider ()
{
  return m_enbrrcSapProvider_new;
}

void
EnbRrcApplication::AddEnb (uint16_t cellId, Ipv4Address enbAddr, Ipv4Address rrcAddr, Ptr<LteEnbRrc> rrc)
{
  NS_LOG_FUNCTION (this << cellId << enbAddr << rrcAddr << rrc);
  EnbInfo enbInfo;
  enbInfo.enbAddr = enbAddr;
  enbInfo.rrcAddr = rrcAddr;
  m_enbInfoByCellId[cellId] = enbInfo;
 enb_ip_rrc_ptr[enbAddr] = rrc;

}


void 
EnbRrcApplication::RecvFromRRCSocket (Ptr<Socket> socket)
{
	NS_LOG_FUNCTION (this << socket);
	  NS_ASSERT (socket == m_rrcSocket);

	  Address from;
		        Ptr<Packet> packet = socket->RecvFrom (from);

if(packet->GetSize() == 33){
        std::cout<<"here receiving roy packet ---- "<<"\n";
}
else{
	Ipv4Address enb_ipv4 = InetSocketAddress::ConvertFrom (from).GetIpv4 ();
	std::map<Ipv4Address, Ptr<LteEnbRrc>>::iterator enbit = enb_ip_rrc_ptr.find (enb_ipv4);
	NS_ASSERT_MSG (enbit != enb_ip_rrc_ptr.end (), "unknown ENB IP " << enb_ipv4);
	Ptr<LteEnbRrc> r = enbit->second;
	NS_LOG_FUNCTION (this << socket << r);
	RntiHeader hd;
	packet->RemoveHeader(hd);
	uint16_t uldl_v = hd.GetUlDl();
	Ptr<Packet> np = packet->Copy();
	switch(uldl_v)
		     {

		      case 0: // FOR ULCCCH MSGS
		      {
		        	NS_LOG_INFO("ENB RRC APP CASE 0 ==== FOR UL CCCH MSGS");
		        	uint16_t rnti_v = hd.GetRntiData();
			        RrcUlCcchMessage rrcUlCcchMessage;
			        packet->PeekHeader (rrcUlCcchMessage);

			        // Declare possible headers to receive
			         RrcConnectionReestablishmentRequestHeader rrcConnectionReestablishmentRequestHeader;
			         RrcConnectionRequestHeader rrcConnectionRequestHeader;

			         // Deserialize packet and call member recv function with appropiate structure
			        switch ( rrcUlCcchMessage.GetMessageType () )
			           {
			           case 0:
			        	   NS_LOG_INFO("AT RRC NODE -- CASE 0 :: Recv RRC CONN RESTABLISHMENT REQUEST");
			             packet->RemoveHeader (rrcConnectionReestablishmentRequestHeader);
			             LteRrcSap::RrcConnectionReestablishmentRequest rrcConnectionReestablishmentRequestMsg;
			             rrcConnectionReestablishmentRequestMsg = rrcConnectionReestablishmentRequestHeader.GetMessage ();
			             r->GetUeManager(rnti_v)->RecvRrcConnectionReestablishmentRequest_new (enb_ipv4,rnti_v,rrcConnectionReestablishmentRequestMsg);
			             break;

			           case 1:
			        	   NS_LOG_INFO("AT RRC NODE -- CASE 1 :: Recv RRC CONN REQUEST");
			             packet->RemoveHeader (rrcConnectionRequestHeader);
			             LteRrcSap::RrcConnectionRequest rrcConnectionRequestMsg;
			             rrcConnectionRequestMsg = rrcConnectionRequestHeader.GetMessage ();
			             r->GetUeManager(rnti_v)->RecvRrcConnectionRequest_new (enb_ipv4,rnti_v,rrcConnectionRequestMsg);
			             break;
			           }
		        }
			        	break;

			        case 1: // for UL DCCCH MSGS
			        {
			        	NS_LOG_INFO("ENB RRC APP CASE 1 ==== UL DCCCH MSGS");
			        	uint16_t rnti_val = hd.GetRntiData();
			        	RrcUlDcchMessage rrcUlDcchMessage;
			        	packet->PeekHeader (rrcUlDcchMessage);

			        	  // Declare possible headers to receive
			        	  MeasurementReportHeader measurementReportHeader;
			        	  RrcConnectionReconfigurationCompleteHeader rrcConnectionReconfigurationCompleteHeader;
			        	  RrcConnectionReestablishmentCompleteHeader rrcConnectionReestablishmentCompleteHeader;
			        	  RrcConnectionSetupCompleteHeader rrcConnectionSetupCompleteHeader;

			        	  // Declare possible messages to receive
			        	  LteRrcSap::MeasurementReport measurementReportMsg;
			        	  LteRrcSap::RrcConnectionReconfigurationCompleted rrcConnectionReconfigurationCompleteMsg;
			        	  LteRrcSap::RrcConnectionReestablishmentComplete rrcConnectionReestablishmentCompleteMsg;
			        	  LteRrcSap::RrcConnectionSetupCompleted rrcConnectionSetupCompletedMsg;

			        	  // Deserialize packet and call member recv function with appropiate structure
			        	  switch ( rrcUlDcchMessage.GetMessageType () )
			        	    {
			        	    case 1:
			        	    	NS_LOG_INFO("AT RRC NODE -- CASE 1 :: Recv MEASUREMENT REPORT");
			        	    	packet->RemoveHeader (measurementReportHeader);
			        	      measurementReportMsg = measurementReportHeader.GetMessage ();
			        	      r->GetUeManager(rnti_val)->RecvMeasurementReport_new (rnti_val,measurementReportMsg);
			        	      break;
			        	    case 2:
			        	    	NS_LOG_INFO("AT RRC NODE -- CASE 2 :: Recv RRC CONN RECONFIG COMPLETE");
			        	      packet->RemoveHeader (rrcConnectionReconfigurationCompleteHeader);
			        	      rrcConnectionReconfigurationCompleteMsg = rrcConnectionReconfigurationCompleteHeader.GetMessage ();
			        	      r->GetUeManager(rnti_val)->RecvRrcConnectionReconfigurationCompleted_new (rnti_val,rrcConnectionReconfigurationCompleteMsg);
			        	      break;
			        	    case 3:
			        	    	NS_LOG_INFO("AT RRC NODE -- CASE 3 :: Recv RRC CONN RESTABLISHMENT COMPLETE");
			        	      packet->RemoveHeader (rrcConnectionReestablishmentCompleteHeader);
			        	      rrcConnectionReestablishmentCompleteMsg = rrcConnectionReestablishmentCompleteHeader.GetMessage ();
			        	      r->GetUeManager(rnti_val)->RecvRrcConnectionReestablishmentComplete (rrcConnectionReestablishmentCompleteMsg);
			        	      break;
			        	    case 4:
			        	    	NS_LOG_INFO("AT RRC NODE -- CASE 4 :: Recv RRC CONN SETUP COMPLETE");
			        	      packet->RemoveHeader (rrcConnectionSetupCompleteHeader);
			        	      rrcConnectionSetupCompletedMsg = rrcConnectionSetupCompleteHeader.GetMessage ();
			        	      r->GetUeManager(rnti_val)->RecvRrcConnectionSetupCompleted (rrcConnectionSetupCompletedMsg);
			        	      break;
			        	    }
			        }
			        	break;


		        }
}
}

void 
EnbRrcApplication::SendToRRCSocket (Ptr<Packet> packet, Ipv4Address enbAddr)
{
  NS_LOG_FUNCTION (this << packet << enbAddr <<m_enbRRCPort);
  m_rrcSocket->SendTo (packet, 0, InetSocketAddress (enbAddr, m_enbRRCPort));
}



}  // namespace ns3
