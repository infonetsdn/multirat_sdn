/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Manuel Requena <manuel.requena@cttc.es>
 */

#ifndef LTE_SPLIT_RRC_HEADER_H
#define LTE_SPLIT_RRC_HEADER_H

#include "ns3/header.h"

#include <list>

namespace ns3 {

////////////////////////////////////////////////////////////////////////////////

//CUSTOM HEADER FOR SPLIT RRC TO SAVE RNTI == IIT BOMBAY

////////////////////////////////////////////////////////////////////////////////

class RntiHeader : public Header
{
public:
	RntiHeader();
	~RntiHeader();

  // new methods
  void SetRntiData (uint16_t data);
  uint16_t GetRntiData (void);

  void SetMsgType (uint16_t data);
  uint16_t GetMsgType (void);

  void SetUlDl (uint16_t data);
  uint16_t GetUlDl (void);

  void Setlcid (uint8_t data);
  uint8_t Getlcid (void);

  void SetRejectWaitTime (uint8_t data);
  uint8_t GetRejectWaitTime (void);

  // new method needed
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  // overridden from Header
  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual void Print (std::ostream &os) const;

private:
  uint16_t rnti_data;
  uint16_t msg_type;
  uint16_t ul_dl_type;
  uint8_t lcid_value;
  uint8_t reject_wait_time;
};


} // namespace ns3

#endif // LTE_SPLIT_RRC_HEADER_H
