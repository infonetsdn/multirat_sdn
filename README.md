============================================================================================================
			MULTI-RAT SDN Simulator -IIT Bombay
============================================================================================================

This multi-RAT SDN code package is built on top of the ns-3 simulator (version 3.26) and provides a framework 
for simulating SDN based multi-RAT network controlling LTE and WLAN together. This package has been created 
with the intent of simulating SDN based multi-RAT networks while keeping the code changes from the base ns-3 
package minimal. In order to use this code repository, "LTE" and "WiFi" folders in the "src" directory of the ns-3 
base implementation must be replaced by the folders provided. The instructions for configuring and building 
the package remains same as that of the ns-3 base package.

A controller node is built and is connected to LTE eNodeBs (eNB) and WLAN APs over IP connections. In this simulator, 
a single controller node is created. The controller consists of LTE controller and WLAN controller as logical entities. 
Other than the above, the following changes have been incorporated in the WLAN and LTE modules to make it SDN compatible.

1.WLAN Module: 
	- The changes for the WLAN module have been made in the file "model/ap-wifi-mac.cc". No separate module
	  has been written for the same.
        - The "Association Request" management frame is no longer processed by WLAN AP.The received frame is retrieved, 
          assembled as a new message and forwarded to the controller over a TCP Socket for processing.
        - The controller accepts the request if the MAC ID of the UE is present in a file (named "log.csv") containing the 
          MAC IDs of the white-listed UEs .
        - All other management messages are handled by the WLAN APs.

2. LTE Module:
        - The changes for the LTE module involve the removal of control functionality from the LTE eNB. As a result RRC 
	  functionality is removed from the eNB and placed in the controller. This has been achieved with the help of new 
          files namely "lte-split-rrc-header.cc, enb-rrc-application.cc, epc-enb-rrc-sap.cc" and their corresponding
	  header files.
        - The "enb-rrc-application.cc" contains the application written at eNB and LTE controller to communicate control 
	  messages such as RRC and measurement messages. The application at eNB intercepts control packets at PDCP layer,reassembles
	  them and forwards to LTE controller over UDP sockets. It also receives a control packet from LTE controller for 
          forwarding over the air interface. The application at LTE controller is coupled with MME. 
        - The file "lte-split-rrc-header.cc" contains the details such as RNTI, type of control message etc., used by the
	  application for reassembly of the message to be sent to the controller.
        - "epc-enb-rrc-sap.cc" provides a service access point between the newly split eNB-RRC and RRC-MME.
        - There are no changes for the data traffic path in the simulator as the data plane remains unaffected.

Note:
 - The simulator makes use of custom interfaces based on UDP/TCP between the controller and modified base stations. In future, we 
would like to integrate this with the OpenFlow functionality.





